console.log("whats up dawg")

var THREE = require('three');
var scene, camera, renderer;
var material0, material1, material2, material3, material4;
var geometry0, geometry1, geometry2, geometry3, geometry4;
var mesh0, mesh1, mesh2, mesh3, mesh4;
var dirLight, start;
// geometry[5],, mesh[5]
function init () {
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(25, window.innerWidth / window.innerHeight, 0.1, 1000);
  camera.position.z = 15;
  geometry0 = new THREE.BoxGeometry(1, 0.5, 1);
  geometry1 = new THREE.BoxGeometry(1, 0.5, 1);
  geometry2 = new THREE.BoxGeometry(1, 0.5, 1);
  geometry3 = new THREE.BoxGeometry(1, 0.5, 1);
  geometry4 = new THREE.BoxGeometry(1, 0.5, 1);

  material0 = new THREE.MeshLambertMaterial({color: 'hsl(10, 50%, 50%)'});
  material1 = new THREE.MeshLambertMaterial({color: 'hsl(0, 54%, 54%)'});
  material2 = new THREE.MeshLambertMaterial({color: 'hsl(0, 58%, 58%)'});
  material3 = new THREE.MeshLambertMaterial({color: 'hsl(0, 62%, 62%)'});
  material4 = new THREE.MeshLambertMaterial({color: 'hsl(0, 66%, 66%)'});

  mesh0 = new THREE.Mesh(geometry0, material0);
  mesh1 = new THREE.Mesh(geometry1, material1);
  mesh2 = new THREE.Mesh(geometry2, material2);
  mesh3 = new THREE.Mesh(geometry3, material3);
  mesh4 = new THREE.Mesh(geometry4, material4);
  scene.add(mesh0);
//  scene.add(mesh1);
//  scene.add(mesh2);
//  scene.add(mesh3);
//  scene.add(mesh4);
  scene.fog = new THREE.Fog(0xf7d9aa, 100, 950);

  start = Date.now();

//  scene.add(new THREE.AmbientLight('hsl(0, 100%, 100%)'));
  dirLight = new THREE.DirectionalLight('hsl(0.50%, 50%)');
  dirLight.position.set(1,1,1);
  scene.add(dirLight);
  renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);
  console.log(mesh0);
  window.addEventListener('resize', onResize);
  window.addEventListener('mousemove', onMouseMove);
}

function animate () {
  requestAnimationFrame(animate);
  mesh0.rotation.x += 0.01;
  mesh0.rotation.y += 0.02;
  mesh1.rotation.x += 0.03;
  mesh1.rotation.y += 0.02;
  mesh2.rotation.x += 0.01;
  mesh2.rotation.y += 0.04;
  mesh3.rotation.x += 0.005;
  mesh3.rotation.y += 0.02;
  mesh4.rotation.x += 0.015;
  mesh4.rotation.y += 0.02;
  mesh0.scale.x = ((((Date.now() - start)) / 900) % 10) - 5;
  mesh1.scale.x = ((((Date.now() - start)) / 800) % 10) - 5;
  mesh2.scale.x = ((((Date.now() - start)) / 700) % 10) - 5;
  mesh3.scale.x = ((((Date.now() - start)) / 600) % 10) - 5;
  mesh4.scale.x = ((((Date.now() - start)) / 1000) % 10) - 5;

  renderer.render(scene, camera);
  //console.log(Date.now() - start);

}

function onResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

function onMouseMove(ev) {
    console.log(ev);
    var x = ev.x;
    var y = ev.y;
    var sc_x = x / window.innerWidth;
    var sc_y = y / window.innherHeight;
    var cl_x = Math.floor(x / window.innerWidth * 200);
    //mesh0.position.x = sc_x;
    //mesh0.position.y = sc_y;
    camera.position.x = sc_x / 10;
    camera.position.y = sc_y / 10;

    var newColor = new THREE.Color(`hsl(${cl_x}, 50%, 50%)`);
    mesh0.material.color = newColor;
}
init();
animate();
